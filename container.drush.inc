<?php

/**
 * @file
 * Drush integration.
 */

/**
 * Implements hook_drush_cache_clear().
 *
 * @internal
 */
function container_drush_cache_clear(array &$types) {
  $types['discovery--annotated-class'] = 'container_flush_cache_discovery__annotated_class';
}
