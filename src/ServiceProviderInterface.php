<?php

namespace Drupal\container;

use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Interface that all service providers must implement.
 */
interface ServiceProviderInterface {

  /**
   * ServiceProviderInterface constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
   *   DI container.
   */
  public function __construct(ContainerBuilder $container);

  /**
   * Work with container before loading the YAMLs.
   */
  public function register();

  /**
   * Final processing of the container.
   */
  public function alter();

}
