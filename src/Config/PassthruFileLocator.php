<?php

namespace Drupal\container\Config;

use Symfony\Component\Config\FileLocator;

/**
 * File locator that allows passing the resource as is.
 */
class PassthruFileLocator extends FileLocator {

  /**
   * {@inheritdoc}
   */
  public function locate($name, $currentPath = NULL, $first = TRUE) {
    return $name;
  }

}
