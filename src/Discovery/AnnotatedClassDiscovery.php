<?php

namespace Drupal\container\Discovery;

use Doctrine\Common\Annotations\SimpleAnnotationReader;
use Drupal\container\Annotation\Annotation;

/**
 * Defines a discovery mechanism to find the annotated classes.
 */
class AnnotatedClassDiscovery {

  /**
   * A cache backend to store the items that has been discovered.
   *
   * @var \DrupalCacheInterface
   */
  protected $cache;
  /**
   * An instance of the "annotation.reader.simple" service.
   *
   * @var \Doctrine\Common\Annotations\SimpleAnnotationReader
   */
  protected $annotationReader;
  /**
   * A list of modules using DI container.
   *
   * @var string[]
   */
  protected $modules = [];
  /**
   * A storage of subpaths and plugins annotations.
   *
   * @var array[]
   */
  protected $subpaths = [];

  /**
   * AnnotatedClassDiscovery constructor.
   *
   * @param \Doctrine\Common\Annotations\SimpleAnnotationReader $annotation_reader
   *   An instance of the "annotation.reader.simple" service.
   * @param string[] $modules
   *   A value of "%modules%" parameter of the container.
   */
  public function __construct(SimpleAnnotationReader $annotation_reader, array $modules) {
    $this->annotationReader = $annotation_reader;
    $this->annotationReader->addNamespace('Doctrine\Common\Annotations\Annotation');
    $this->annotationReader->addNamespace('Symfony\Component\Validator\Constraints');
    $this->modules = $modules;
    $this->cache = _cache_get_object('cache_discovery__annotated_class');
  }

  /**
   * Sets a class of annotation to check classes for.
   *
   * @param string $annotation_class
   *   A fully qualified path to annotation class.
   */
  protected function validateAnnotationClass($annotation_class) {
    if (!class_exists($annotation_class)) {
      throw new \InvalidArgumentException(sprintf('The "%s" class does not exists.', $annotation_class));
    }

    if (!is_a($annotation_class, Annotation::class, TRUE)) {
      throw new \InvalidArgumentException(sprintf('The annotation class must be an instance of "%s".', Annotation::class));
    }

    $annotation_namespace = explode('\\', $annotation_class);
    array_pop($annotation_namespace);

    $this->annotationReader->addNamespace(implode('\\', $annotation_namespace));
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions($subpath, $annotation_class) {
    $directories = [];
    $definitions = [];

    $this->validateAnnotationClass($annotation_class);

    foreach ($this->modules as $module => $src) {
      $src = "$src/src";

      if (is_dir("$src/$subpath")) {
        $directories[$module][$src] = "$src/$subpath";
      }
    }

    if (isset($this->subpaths[$subpath]) && $annotation_class !== $this->subpaths[$subpath]) {
      watchdog('container', 'The "@subpath" subpath is already reserved for plugins, annotated by the "@annotation1", and you are trying to combine them with "@annotation2".', [
        '@subpath' => $subpath,
        '@annotation1' => $this->subpaths[$subpath],
        '@annotation2' => $annotation_class,
      ], WATCHDOG_ERROR);
    }
    else {
      $this->subpaths[$subpath] = $annotation_class;
    }

    foreach ($directories as $module => $paths) {
      foreach ($paths as $src => $path) {
        $cache_id = md5($path . $annotation_class);
        $cache = $this->cache->get($cache_id);

        if (FALSE !== $cache && isset($cache->data)) {
          $definitions += $cache->data;
          continue;
        }

        $sorted = [];
        $list = [];

        /* @var \SplFileInfo $file */
        foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, \RecursiveDirectoryIterator::SKIP_DOTS)) as $file) {
          $extension = $file->getExtension();

          if ('php' === $extension) {
            $class = "Drupal\\$module" . str_replace('/', '\\', str_replace([$src, ".$extension"], '', $file));
            /* @var \Drupal\container\Annotation\Annotation $annotation */
            $annotation = $this->annotationReader->getClassAnnotation(new \ReflectionClass($class), $annotation_class);

            if (NULL !== $annotation) {
              $annotation->setClass($class);
              $annotation->setProvider($module);

              $id = $annotation->getId();

              $sorted[$id] = $annotation->priority;
              $list[$id] = $annotation->get();
            }
          }
        }

        if (!empty($list)) {
          array_multisort($sorted, SORT_DESC);

          foreach ($sorted as $id => $priority) {
            $sorted[$id] = $list[$id];
          }

          $definitions += $sorted;

          $this->cache->set($cache_id, $sorted);
        }
      }
    }

    return $definitions;
  }

}
