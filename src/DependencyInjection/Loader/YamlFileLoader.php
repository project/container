<?php

namespace Drupal\container\DependencyInjection\Loader;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader as YamlFileLoaderBase;
use Drupal\container\Config\PassthruFileLocator;

/**
 * YAML file loader that uses resources without locating them previously.
 */
class YamlFileLoader extends YamlFileLoaderBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(ContainerBuilder $container) {
    parent::__construct($container, new PassthruFileLocator());
  }

}
