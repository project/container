<?php

namespace Drupal\container\Annotation;

/**
 * Base annotation.
 *
 * @Annotation
 */
class Annotation {

  /**
   * Unique ID of this annotation.
   *
   * @var string
   *
   * @Required
   */
  public $id;
  /**
   * The priority of the plugin among the others.
   *
   * @var int
   */
  public $priority = 0;
  /**
   * The class of this annotation.
   *
   * @var string
   */
  protected $class;
  /**
   * The name of module providing this annotation.
   *
   * @var string
   */
  protected $provider;

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getClass() {
    return $this->class;
  }

  /**
   * {@inheritdoc}
   */
  public function setClass($class) {
    $this->class = $class;
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return $this->provider;
  }

  /**
   * {@inheritdoc}
   */
  public function setProvider($provider) {
    $this->provider = $provider;
  }

  /**
   * Returns annotation instance.
   */
  public function get() {
    return $this;
  }

}
