<?php

namespace Drupal\container;

use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * The base service provider to inherit from.
 */
abstract class ServiceProvider implements ServiceProviderInterface {

  /**
   * DI container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerBuilder
   */
  protected $container;

  /**
   * {@inheritdoc}
   */
  public function __construct(ContainerBuilder $container) {
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public function register() {
  }

  /**
   * {@inheritdoc}
   */
  public function alter() {
  }

}
