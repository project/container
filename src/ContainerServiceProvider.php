<?php

namespace Drupal\container;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Service provider.
 */
class ContainerServiceProvider extends ServiceProvider {

  /**
   * {@inheritdoc}
   */
  public function alter() {
    $annotation_reader = $this->container->getDefinition('annotation.reader.simple');

    foreach ($this->container->getParameter('modules') as $module => $path) {
      $directory = "$path/src/Annotation";

      if (is_dir($directory)) {
        $annotation_reader->addMethodCall('addNamespace', ["Drupal\\$module\\Annotation"]);
      }
    }

    // Avoid using ContainerBuilder::findTaggedServiceIds() as that we result in
    // additional iterations around all the service definitions.
    foreach ($this->container->getDefinitions() as $consumer_id => $definition) {
      $tags = $definition->getTags();

      if (isset($tags['service_collector'])) {
        foreach ($tags['service_collector'] as $pass) {
          $this->buildServiceCollectorFactories($pass, $consumer_id, $definition);
        }
      }
    }
  }

  /**
   * Builds services factories.
   *
   * @param array $pass
   *   The service collector pass data.
   * @param string $consumer_id
   *   The consumer service ID.
   * @param \Symfony\Component\DependencyInjection\Definition $consumer
   *   The definition of the consumer.
   *
   * @example
   * @code
   * // Factory.
   * tags:
   *   - name: service_collector
   *     tag: breadcrumb_builder
   *     call: addBuilder
   *
   * // Handler.
   * tags:
   *   - name: breadcrumb_builder
   *     priority: 100
   * @endcode
   */
  protected function buildServiceCollectorFactories(array $pass, $consumer_id, Definition $consumer) {
    $tag = isset($pass['tag']) ? $pass['tag'] : $consumer_id;
    $class = $consumer->getClass();
    $method = isset($pass['call']) ? $pass['call'] : 'addHandler';
    $params = (new \ReflectionMethod($class, $method))->getParameters();

    if (empty($params[0]) || empty($params[0]->getClass())) {
      throw new \LogicException(sprintf('Service consumer "%s" class method %s::%s() has to type-hint an interface.', $consumer_id, $class, $method));
    }

    $interface = $params[0]->getClass()->getName();
    $handlers = [];

    foreach ($this->container->findTaggedServiceIds($tag) as $id => $attributes) {
      if (!is_subclass_of($this->container->getDefinition($id)->getClass(), $interface)) {
        throw new \LogicException(sprintf('Service "%s" for consumer "%s" does not implement "%s".', $id, $consumer_id, $interface));
      }

      $handlers[$id] = isset($attributes[0]['priority']) ? $attributes[0]['priority'] : 0;
    }

    if (!empty($pass['required']) && empty($handlers)) {
      throw new \LogicException(sprintf('At least one service tagged with "%s" is required.', $tag));
    }

    // Sort all handlers by priority.
    arsort($handlers, SORT_NUMERIC);

    foreach ($handlers as $id => $priority) {
      $consumer->addMethodCall($method, [new Reference($id), $id, $priority]);
    }
  }

}
