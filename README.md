# Container

Are you missing the Symfony's dependency injection container? This is what you're looking for!

## Usage

To start using this package you must [install it via Composer](https://www.drupal.org/node/2718229). Thereafter add the next line to the `*.info` file of the module which is going to populate dependencies to the container:

```ini
container = TRUE
```

Now you're good to go and declare services in a `*.services.yml` within the module (exactly the same as in Drupal 8).

Besides of that, every module can have a service provider, which must implement the `Drupal\container\ServiceProviderInterface`, be named as `MyModuleNameServiceProvider` and be in the `Drupal\my_module_name` namespace (again, [the same as in Drupal 8](https://www.drupal.org/docs/8/api/services-and-dependency-injection/altering-existing-services-providing-dynamic-services)).

### Important recommendation

Set the following configuration to your **development settings** to reflect the changes of services in YMLs and/or service providers immediately:

```php
// Keep DI container up to date during development.
$conf['container_debug'] = TRUE;
```

### What is in the container out of the box?

- The `modules` parameter which contains a list of modules that uses the container (keys are module names, values - relative paths).
- The `validator` service allowing you to use [the Validator Component of Symfony](https://symfony.com/doc/current/components/validator.html).
- The `annotation.reader.simple` and the `annotation.discovery` services.

A bare minimum, as you can see.

### Factory of tagged services

The definition inside of the YAML.

```yml
services:
  name.factory:
    class: Drupal\module_name\Factory
    tags:
      - name: service_collector
        call: addChild
        tag: factory_identifier

  name.factory.child:
    class: Drupal\module_name\FactoryChild
    tags:
      - name: factory_identifier
        priority: 0
```

As a result, all services having the `factory_identifier` tag will be passed to `addChild` method of the `name.factory`.
